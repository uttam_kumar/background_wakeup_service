package com.example.uttam.backgroundwakeup;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("TAG", "onReceive");

        if ("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
           // while (true) {

                // This intent is used to start background service. The same service will be invoked for each invoke in the loop.
                //context.startService(startServiceIntent);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    Intent startServiceIntent = new Intent(context, MyBackgroundService.class);
                    context.startForegroundService(startServiceIntent);
                    Log.d("TAG", "startForegroundService");
                } else {
                    Intent startServiceIntent = new Intent(context, MyBackgroundService.class);
                    context.startService(startServiceIntent);
                    Log.d("TAG", "startService");
                }

                // Current thread will sleep one second.
                //Thread.sleep(10000);
           // }

        }
    }
}